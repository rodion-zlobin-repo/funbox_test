import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Shop from './js/components/Shop';

class App extends Component {
  state = {
    items: [
      {
        code: 1,
        with: 'с фуа-гра',
        description: 'Печень утки разварная с артишоками.',
        portions: '10',
        gift: 'мышь',
        weight: '0,5',
        enabled: true,
        selected: false,
        change: false
      },
      {
        code: 2,
        with: 'с рыбой',
        description: 'Головы щучьи с чесноком да свежайшая сёмгушка.',
        portions: '40',
        gift: '2 мыши',
        weight: '2',
        enabled: true,
        selected: true,
        change: false
      },
      {
        code: 3,
        with: 'с курой',
        description: 'Филе из цеплят с трюфелями в бульоне.',
        portions: '100',
        gift: '5 мышей',
        weight: '5',
        enabled: false,
        selected: true,
        change: false
      }
    ]
  }
// todo: refactore code, change hover logic
  itemState = (code) => {
    this.setState({
      items: this.state.items.map(item => {
        if(item.code === code && item.enabled) {
          item.selected = !item.selected;
        }
        return item;
      })
    });
  }
  itemHover = (code) => {
    this.setState({
      items: this.state.items.map(item => {
        if(item.code === code && item.selected) {
          item.change = !item.change;
        }
        return item;
      })
    });
  }
  render() {
    return (
      <div className='container'>
        <h1>Ты сегодня покормил кота?</h1>
        <div className='row'>
          <Shop items={this.state.items} itemState={this.itemState} itemHover={this.itemHover} />
        </div>
      </div>
    );
  }
}

export default App;
