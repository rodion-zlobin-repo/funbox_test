import React, { Component } from 'react';

class Item extends Component {
  render() {
    {/*todo: make link*/}
    const itemMessage = this.props.item.enabled 
      ? `Чего сидишь? Порадуй котэ, купи.`
      : `Печалька, ${this.props.item.with} закончился`;
    return (

      <div className='col'>

        {/*item*/}
        <div 
          className={this.props.item.enabled
            ? this.props.item.selected 
              ? 'item item--checked'
              : 'item'
            : 'item item--disabled'
          }
        >

            {/*card*/}
            <div className='card'
              onClick={this.props.theItemState.bind(this, this.props.item.code)}
              onMouseEnter={this.props.theHover.bind(this, this.props.item.code)}
              onMouseLeave={this.props.theHover.bind(this, this.props.item.code)}
            >
              {/*card header*/}
              <div className='card__header'>
                <h6>
                  {/*todo: add disabled state item behavior*/}
                  { this.props.item.enabled
                      ? !this.props.item.change
                        ? `Сказочное заморское яство`
                        : `Котэ не одобряет?`
                      : `Сказочное заморское яство`
                  }

                </h6>
              </div>
              {/*card body*/}
              <div className='card__body'>
                <h2 className='card__title'>Нямушка
                  <em>
                    {`${this.props.item.with}`}
                  </em>
                </h2>
                <div>
                  <p className='card__portions'>
                    <strong>
                      {`${this.props.item.portions}`}
                      {` `}
                    </strong>
                    {`порций`}
                  </p>
                  <p className='card__portions'>
                    <strong>
                      {/*todo: divide gift*/}
                      {`${this.props.item.gift}`}
                      {` `}
                    </strong>
                    {`в подарок`}
                  </p>
                  <p className='card__opinion'>
                    {
                      !this.props.item.enabled 
                      ? 'заказчик доволен'
                      : false
                    }
                  </p>
                </div>
              </div>
              {/*card image*/}
              <img className='card__img' src='./images/cat1.png' />
              {/*card footer*/}
              <div className='card__footer'>
                <span className='card__badge badge badge--circle'>
                  <span className='card__weight'>
                    {`${this.props.item.weight}`}
                  </span>
                  <span className='card__units'>
                    {`кг`}
                  </span>
                </span>
              </div>
          </div>

          {/*item description*/}
          <div className='item__description'>
            {(this.props.item.selected 
              && this.props.item.enabled) 
              ? this.props.item.description
              : itemMessage}
          </div>
        </div>

      </div>
    );
  }
}

export default Item;
