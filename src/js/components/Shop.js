import React, { Component } from 'react';
import Item from './Item';

class Shop extends Component {
  render() {
    return this.props.items.map((item) => (
      <Item item={item} key={item.code} theItemState={this.props.itemState} theHover={this.props.itemHover}/>
    ));
  }
}

export default Shop;
